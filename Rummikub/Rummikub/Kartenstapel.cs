﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rummikub
{
    public class Kartenstapel
    {
        public Kartenstapel() { }

        public List<Spielstein> Spielsteine { get; set; } = new List<Spielstein>();

        public void Add(Spielstein spielstein)
        {
            Spielsteine.Add(spielstein);
        }

        public void printKartenstapel()
        {
            foreach (Spielstein s in Spielsteine)
            {
                s.print();
            }
        }

        public void Mischen()
        {
            
            
        }



    }
}
