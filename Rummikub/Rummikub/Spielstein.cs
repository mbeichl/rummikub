﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rummikub
{
    /// <summary>
    /// </summary>
    public  class Spielstein
    {
        public Spielstein() { }

        public Spielstein(int number, Color color)
        {
            Number = number;
            Color = color;
        }

        [Range(1, 13)]
        public int Number { get; set; }
        public Color Color { get; set; }


        public void print() {

            //Farbe vorbereiten für cosole print des spielsteins
            switch (Color)
            {
                case Color.Black: Console.BackgroundColor = ConsoleColor.Black;
                    break;

                case Color.Red: Console.BackgroundColor = ConsoleColor.Red;
                    break;
                case Color.Orange:
                    Console.BackgroundColor = ConsoleColor.DarkYellow;
                    break;

                case Color.Blue:
                    Console.BackgroundColor = ConsoleColor.Blue;
                    break;
            }

            Console.WriteLine(Number + "  ");

        }
    }

    
}
