﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rummikub
{
    public class Spiel
    {

        public Spiel(int anzahlMitspieler) {

         this.AnazhlMitspieler = anzahlMitspieler;

            for(int i = 1; i <= 13; i++)
            {
                Kartenstapel.Add(new Spielstein(i,Color.Orange));
                Kartenstapel.Add(new Spielstein(i, Color.Orange));

                Kartenstapel.Add(new Spielstein(i, Color.Blue));
                Kartenstapel.Add(new Spielstein(i, Color.Blue));

                Kartenstapel.Add(new Spielstein(i, Color.Red));
                Kartenstapel.Add(new Spielstein(i, Color.Red));

                Kartenstapel.Add(new Spielstein(i, Color.Black));
                Kartenstapel.Add(new Spielstein(i, Color.Black));

            }


        }


        public int AnazhlMitspieler { get; set; }
        /// <summary>
        /// gelegte Steine
        /// </summary>
        public List<List<Spielstein>> Spielfeld { get; set; } = new List<List<Spielstein>>();

        /// <summary>
        /// Haufen von steinen zum neue Steine ziehen
        /// </summary>
        public Kartenstapel Kartenstapel { get; set; } = new Kartenstapel();


        
     

    }
}
